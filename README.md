# LiveSplit Java bindings
JNA bindings for [livesplit-core](https://github.com/LiveSplit/livesplit-core)

These are the bindings for version 0.11.0, released May 14, 2019.
